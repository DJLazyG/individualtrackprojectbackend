package com.example.demo.service;

import com.example.demo.dao.EventDao;
import com.example.demo.model.Event;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class EventService {

    private final EventDao eventDao;

    @Autowired
    public EventService(@Qualifier("eventBean") EventDao eventDao) {
        this.eventDao = eventDao;
    }

    public int addEvent(Event event){
        return eventDao.insertEvent(event);
    }

    public List<Event> getAllEvents(){
        return eventDao.selectAllEvents();
    }

    public Optional<Event> getEventById(UUID id){
        return eventDao.selectEventById(id);
    }

    public int deleteEvent(UUID id){
        return eventDao.deleteEventById(id);
    }

    public int updateEvent(UUID id, Event newEvent){
        return eventDao.updateEventById(id,newEvent);
    }
}
