package com.example.demo.api;

import com.example.demo.dao.TicketDao;
import com.example.demo.dao.UserDao;
import com.example.demo.model.Ticket;
import com.example.demo.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.lang.NonNull;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("api/tickets")
@RestController
public class TicketController {

    @Autowired
    TicketDao ticketDao;
    @Autowired
    UserDao userDao;


    @GetMapping()
    public List<Ticket> GetAllTickets(){
        return ticketDao.findAll();
    }

    @GetMapping(path="{id}")
    public Optional<Ticket> getTicketById(@PathVariable("id") Long aLong){
        return ticketDao.findById(aLong);
    }

    @PostMapping
    public void createTicket(@Valid @NonNull @RequestBody Ticket ticket){
        
        ticketDao.save(ticket);
    }

    @PostMapping(path = "{id}",consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE,
            produces = {MediaType.APPLICATION_ATOM_XML_VALUE, MediaType.APPLICATION_JSON_VALUE})
    @PreAuthorize("hasRole('USER') or hasRole('MODERATOR') or hasRole('ADMIN')")
    public void AssignTicket(@PathVariable("id")Long userId, HttpServletRequest request){

//        String username = requestUser.getUsername();
        Long ticketId = Long.parseLong(request.getParameter("id"));
        User user = userDao.findById(userId).orElseThrow(() -> new RuntimeException("Error: User is not found."));
        Set<Ticket> tickets = new HashSet<>();
        Ticket ticket = ticketDao.findTicketById(ticketId);
        tickets.add(ticket);
        user.setTickets(tickets);
    }

    @DeleteMapping(path = "{id}")
    public void deleteTicketById(@PathVariable("id")Long aLong){
        ticketDao.deleteById(aLong);
    }

//    @PutMapping(path={"id"})
    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public void updateTicketById(@PathVariable("id") Long id, @Valid @NonNull @RequestBody Ticket ticket){
        ticketDao.deleteById(id);
        ticketDao.save(ticket);
    }

//    private final TicketService ticketService;
//
//    @Autowired
//    public TicketController(TicketService ticketService) {
//        this.ticketService = ticketService;
//    }
//
//    @PostMapping
//    public void addTicket(@Valid @NonNull @RequestBody Ticket ticket){
//        ticketService.addTicket(ticket);
//    }
//
//    @GetMapping
//    public List<Ticket> getAllTickets(){
//        return ticketService.getAllTickets();
//    }
//
//    @GetMapping(path = "{id}")
//    public Ticket getTicketById(@PathVariable("id") UUID id){
//        return ticketService.getTicketById(id)
//                .orElse(null);
//    }
//
//    @DeleteMapping(path = "{id}")
//    public void deleteTicketById(@PathVariable("id") UUID id){
//        ticketService.deleteTicket(id);
//    }
//
//    @PutMapping(path="{id}")
//    public void updateTicket(@PathVariable("id") UUID id,@Valid @NonNull @RequestBody Ticket ticketToUpdate){
//        ticketService.updateTicket(id,ticketToUpdate);
//    }
}
