package com.example.demo.api;


import com.example.demo.dao.RoleDao;
import com.example.demo.dao.UserDao;
import com.example.demo.model.ERole;
import com.example.demo.model.Person;
import com.example.demo.model.Role;
import com.example.demo.model.User;
import net.bytebuddy.implementation.bytecode.Throw;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/api/users")
@RestController
public class UserController {
    @Autowired
    UserDao userDao;

    @Autowired
    RoleDao roleDao;

    @GetMapping(path="{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public Optional<User> getUserById(@PathVariable("id") Long aLong){
         return userDao.findById(aLong);
    }

    @GetMapping("/all")
    @PreAuthorize("hasRole('ADMIN')")
    public List<User> GetAllUsers(){
        return userDao.findAll();
    }

    @DeleteMapping(path = "{id}")
    @PreAuthorize("hasRole('ADMIN')")
    public void deleteUserById(@PathVariable("id") Long aLong){
        userDao.deleteById(aLong);
    }

    @RequestMapping(value = "{id}", method = RequestMethod.PUT)
    @PreAuthorize("hasRole('ADMIN')")
    public void updateUserById(@PathVariable("id") Long userId){
        User user=userDao.findById(userId).orElseThrow(() -> new RuntimeException("Error: User is not found."));
        Set<Role> strRoles = user.getRoles();
        if (strRoles.contains(ERole.ROLE_USER)){
            Role userRole = roleDao.findByName(ERole.ROLE_MODERATOR).orElseThrow(() -> new RuntimeException("Error: Role is not found."));
            strRoles.add(userRole);
        }else if(strRoles.contains(ERole.ROLE_MODERATOR)){
            throw new RuntimeException("ERROR: this user already is a moderator.");
        }else{
            throw new RuntimeException("Something is horribly wrong");
        }

        user.setRoles(strRoles);
        userDao.save(user);
    }
}
