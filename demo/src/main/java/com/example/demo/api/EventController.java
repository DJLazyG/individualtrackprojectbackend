package com.example.demo.api;

import com.example.demo.model.Event;
import com.example.demo.model.Ticket;
import com.example.demo.service.EventService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/api/event")
@RestController


public class EventController {

    private final EventService eventService;

    //@Autowired
    public EventController(EventService eventService) {
        this.eventService = eventService;
    }

    @PostMapping
    public void addEvent(@Valid @NonNull @RequestBody Event event){
        eventService.addEvent(event);
    }


    @GetMapping

    public List<Event> getAllEvents(){
        return eventService.getAllEvents();
    }

    @GetMapping(path = "{id}")
    public Event getEventById(@PathVariable("id") UUID id){
        return eventService.getEventById(id)
                .orElse(null);
    }

    @DeleteMapping(path = "{id}")
    public void deleteEventById(@PathVariable("id") UUID id){
        eventService.deleteEvent(id);
    }

    @PutMapping(path="{id}")
    public void updateEvent(@PathVariable("id") UUID id,@Valid @NonNull @RequestBody Event eventToUpdate){
        eventService.updateEvent(id,eventToUpdate);
    }
}
