package com.example.demo.model;

import javax.validation.constraints.NotBlank;
import java.util.UUID;

public class Event {

    private final UUID id;

    @NotBlank
    private final String name;

    @NotBlank
    private final String description;

    @NotBlank
    private final String pictureName;

    public UUID getId() {
        return id;
    }

    public String getPictureName() {
        return pictureName;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Event(UUID id, @NotBlank String name, @NotBlank String description, @NotBlank String pictureName) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.pictureName = pictureName;
    }
}
