package com.example.demo.dao;

import com.example.demo.model.Ticket;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface TicketDao extends JpaRepository<Ticket, Long> {

    Ticket findTicketById(Long aLong);

    @Override
    List<Ticket> findAll();

    @Override
    <S extends Ticket> S saveAndFlush(S s);

    @Override
    boolean existsById(Long aLong);

    @Override
    void deleteById(Long aLong);

    //    int insertTicket(UUID id, Ticket ticket);
//
//    default int insertTicket(Ticket ticket){
//        UUID id = UUID.randomUUID();
//        return insertTicket(id, ticket);
//    }
//
//    List<Ticket> selectAllTickets();
//
//    Optional<Ticket> selectTicketById(UUID id);
//
//    int deleteTicketById(UUID id);
//
//    int updateTicketById(UUID id, Ticket ticket);
}
