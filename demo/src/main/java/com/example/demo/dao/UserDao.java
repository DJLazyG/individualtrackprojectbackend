package com.example.demo.dao;

import java.util.List;
import java.util.Optional;

import com.example.demo.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserDao extends JpaRepository<User, Long> {
    Optional<User> findByUsername(String username);


    @Override
    List<User> findAll();

    @Override
    <S extends User> S saveAndFlush(S s);

    @Override
    void deleteById(Long aLong);

    Boolean existsByUsername(String username);

    Boolean existsByEmail(String email);


}
