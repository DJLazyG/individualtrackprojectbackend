//package com.example.demo.dao;
//
//import com.example.demo.model.Person;
//import com.example.demo.model.Ticket;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.jdbc.core.JdbcTemplate;
//import org.springframework.stereotype.Repository;
//
//import java.util.List;
//import java.util.Optional;
//import java.util.UUID;
//
//@Repository("ticketBean")
//public class TicketDataAccessService {//implements TicketDao
//
//    private final JdbcTemplate jdbcTemplate;
//
//    @Autowired
//    public TicketDataAccessService(JdbcTemplate jdbcTemplate) {
//        this.jdbcTemplate = jdbcTemplate;
//    }
//
//
//    //@Override
//    public int insertTicket(UUID id, Ticket ticket) {
//        String sql= "INSERT INTO ticket VALUES (?,?,?); ";
//        return jdbcTemplate.update(sql,
//                id,
//                ticket.getName(),
//                ticket.getPrice()
//        );
//    }
//
//    //@Override
//    public List<Ticket> selectAllTickets() {
//        String sql="SELECT * FROM ticket";
//        List<Ticket> tickets = jdbcTemplate.query(sql, (resultSet, i) -> {
//            UUID id = UUID.fromString(resultSet.getString("id"));
//            String name =resultSet.getString("name");
//            int price =resultSet.getInt("price");
//            return new Ticket(id, name, price);
//        });
//        return tickets;
//    }
//
//    //@Override
//    public Optional<Ticket> selectTicketById(UUID id) {
//        String sql="SELECT * FROM ticket WHERE id=?";
//        Ticket ticket = jdbcTemplate.queryForObject(
//                sql,
//                new Object[]{id},//can add more parameters if needed by the method
//                (resultSet, i) -> {
//                    Long ticketId = Long.fromString(resultSet.getString("id"));
//                    String name =resultSet.getString("name");
//                    int price = resultSet.getInt("price");
//                    return new Ticket(ticketId, name, price);
//                });
//        return Optional.ofNullable(ticket);
//    }
//
//    //@Override
//    public int deleteTicketById(UUID id) {
//        String sql="DELETE FROM ticket WHERE id=?";
//
//        return jdbcTemplate.update(sql,id);
//    }
//
//    //@Override
//    public int updateTicketById(UUID id, Ticket ticket) {
//        String sql = "UPDATE ticket SET id=?, name = ?,price = ?";
//        return jdbcTemplate.update(sql,
//                id,
//                ticket.getName(),
//                ticket.getPrice()
//        );
//    }
//}
