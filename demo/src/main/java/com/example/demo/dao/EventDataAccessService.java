package com.example.demo.dao;

import com.example.demo.model.Event;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository("eventBean")
public class EventDataAccessService implements EventDao{

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public EventDataAccessService(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public int insertEvent(UUID id, Event event) {
        String sql= "INSERT INTO event VALUES (?,?,?,?); ";
        return jdbcTemplate.update(sql,
                id,
                event.getName(),
                event.getDescription(),
                event.getPictureName()
        );
    }

    @Override
    public List<Event> selectAllEvents() {
        String sql="SELECT * FROM event";
        List<Event> events = jdbcTemplate.query(sql, (resultSet, i) -> {
            UUID id = UUID.fromString(resultSet.getString("id"));
            String name =resultSet.getString("name");
            String description =resultSet.getString("description");
            String pictureName =resultSet.getString("pictureName");
            return new Event(id, name, description, pictureName);
        });
        return events;
    }

    @Override
    public Optional<Event> selectEventById(UUID id) {
        String sql="SELECT * FROM event WHERE id=?";
        Event event = jdbcTemplate.queryForObject(
                sql,
                new Object[]{id},//can add more parameters if needed by the method
                (resultSet, i) -> {
                    UUID ticketId = UUID.fromString(resultSet.getString("id"));
                    String name =resultSet.getString("name");
                    String description =resultSet.getString("description");
                    String pictureName =resultSet.getString("pictureName");
                    return new Event(id, name, description, pictureName);
                });
        return Optional.ofNullable(event);
    }

    @Override
    public int deleteEventById(UUID id) {
        String sql="DELETE FROM event WHERE id=?";

        return jdbcTemplate.update(sql,id);
    }

    @Override
    public int updateEventById(UUID id, Event event) {
        String sql = "UPDATE event SET name = ?,description = ?, pictureName=? WHERE id=?";
        return jdbcTemplate.update(sql,

                event.getName(),
                event.getDescription(),
                event.getPictureName(),
                id
        );
    }
}
