package com.example.demo.dao;

import com.example.demo.model.Person;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
@Repository("postgres")
public class PersonDataAccessService implements PersonDao{

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public PersonDataAccessService(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public int insertPerson(UUID id, Person person) {
        String sql= "INSERT INTO person VALUES (?,?,?,?,?); ";
        return jdbcTemplate.update(sql,
                id,
                person.getName(),
                person.getPassword(),
                person.getEmail(),
                person.isAdmin()
        );
    }

    @Override
    public List<Person> selectAllPeople() {
        String sql="SELECT * FROM person";
        List<Person> people = jdbcTemplate.query(sql, (resultSet, i) -> {
            UUID id = UUID.fromString(resultSet.getString("id"));
            String name =resultSet.getString("name");
            String password =resultSet.getString("password");
            String email =resultSet.getString("email");
            boolean isAdmin = resultSet.getBoolean("isAdmin");
            return new Person(id, name, password, email, isAdmin);
        });
        return people;
    }

    @Override
    public Optional<Person> selectPersonById(UUID id) {
        String sql="SELECT * FROM person WHERE id=?";
        Person person = jdbcTemplate.queryForObject(
                sql,
                new Object[]{id},//can add more parameters if needed by the method
                (resultSet, i) -> {
            UUID personId = UUID.fromString(resultSet.getString("id"));
            String name =resultSet.getString("name");
            String password =resultSet.getString("password");
            String email =resultSet.getString("email");
            boolean isAdmin = resultSet.getBoolean("isAdmin");
            return new Person(personId, name, password, email, isAdmin);
        });
        return Optional.ofNullable(person);
    }

    @Override
    public int deletePersonById(UUID id) {
        String sql="DELETE FROM person WHERE id=?";

        return jdbcTemplate.update(sql,id);
    }

    @Override
    public int updatePersonById(UUID id, Person person) {
        String sql = "UPDATE person SET id=?, name = ?, password = ?, email=?, isadmin=?";
        return jdbcTemplate.update(sql,
                id,
                person.getName(),
                person.getPassword(),
                person.getEmail(),
                person.isAdmin()
        );
    }
}
